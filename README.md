I Denne obligen har følgende studenter samarbeidet: Cato Findalen, Abdi Moohamued og Daniel Siqveland. Daniel gjorde oppgave 1 forskjellig fra Abdi Moohamued og Cato Findalen ellers er resterende oppgaver gjort
sammen.

Abdi Moohamued,   Studentnr: 473127 
Cato Findalen,    Studentnr: 480236
Daniel Siqveland, Studentnr: 473126

Vi brukte diverse verktøy for å kvalitetsikre koden som vi fant på Githuben til Erik.
https://github.com/NTNUcourses/opsys/blob/master/README-C-code-quality.txt  

Gcc(GNU Compiler Collection) var en av de mest brukte, blant for å kompilere c code. 
Her er noen av kommandoene vi brukte:
 "gcc -Wall -pthread -o navnPåKjørbarFil(egentlig kalle hva du vil) navnPå.C-Kode for å kunne lage kjørebar fil. -Wall= warning all. 

CPPCHECK: 
Vi installerte apt-get install cppcheck og kjørte kommandoen: cppcheck: cppcheck --enable=all ./obligprocess1.c

Vi Fikk "(information) Cppcheck cannot find all the include files (use --check-config for details)" på alle, men det var ikke 
på grunn av feil i koden.
Vi Fikk f.eks også "[producerconsumer/producer_consumer_siqveland.c:61]: (style) The scope of the variable 'j' can be reduced."
Men dette så vi ikke på som en kritisk feil så da har vi ikke gjort noe med det.

Clang-tidy-5.0- 
Dette er linter verktøyet vi brukte til å finne feil angående koden og oppsettet.
Kommandoen for å installere: apt-get install clang-tidy-5.0
-checks='*' obligprocess1.c -- -std=c11
Her reagerte den på at include fila <stddef.h>.Dette tror vi ikke har med koden å gjøre

Valgrind 
Dette er det siste verktøyet vi brukte og det var for å sjekke dynamiske aspekter ved koden.

apt-get install valgrind. Kommandoen vi brukte for å installere valgrind
-valgrind --leak-check=yes ./obligprocess1.c . På denne kommando fikk vi ikke noe error, den sjekket om allokert minne blir frigitt.
-valgrind --leak-tool=helgrind ./obligprocess1.c  sjekker om API blir brukte på en riktig måte


  
