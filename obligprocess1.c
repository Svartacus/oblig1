#include<stdio.h> /*printf */
#include <stdlib.h> /* exit  */
#include <unistd.h> /* fork */
#include<sys/wait.h> /* waitpid  */
#include <sys/types.h> /* pid_t */

void process (int number, int time){
printf ("Prosess %d kjører\n", number); 
sleep(time); 
printf("Prosess %d kjørte i %d sekunder\n", number , time); 
}
int main (void) {
int pid; 
pid=fork(); 
if(pid==0){
	process(0,1);
	int pid1=fork();

	if(pid1==0){
		process(1,2);
		exit(0);
	}else{
		process(4,3); 
	waitpid(pid1, NULL , 0);
		int pid2=fork();
			if(pid2 == 0){
			process(5,3);
			exit(0);
			} else {
			waitpid(pid2, NULL, 0);
		}
	}
}

else{
process(2,3);

	int pid3=fork();
	if(pid3==0){

	process(3,2);
	exit(0);
	}else {

	waitpid(pid3, NULL, 0);
	}
waitpid(pid, NULL ,0);

}


return 0; 
}
